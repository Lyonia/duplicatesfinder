﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;

namespace DuplicatesFinder
{
    class Program
    {
        static void Main(string[] args)
        {
            int MenChoise = 0; //Ini value for menu void
            DispMenu(MenChoise); //Ini public static void DispMenu(int choise)
        }


        public static void ManualInput()
        {
            try
            {
                Console.WriteLine("Please type directory path: ");
                string InpPath = Convert.ToString(Console.ReadLine());//Reading of inp value
                Console.WriteLine();
                Console.WriteLine("Was typed PATH: '{0}'", InpPath);
                Console.WriteLine();

                FindDuplicates(InpPath);//Execution of main function
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Exception happen in Manual input");
                Console.ReadLine();
            }
        }


        //Get files recursivelly from folder
        //and subfolders
        public static List<string> GetFiles(string path)
        {
            var files = new List<string>(); //New Files List

            try
            {   // Add range of files from path
                files.AddRange(Directory.GetFiles(path, "*.*",
                                    SearchOption.TopDirectoryOnly));
                foreach (var directory in Directory.GetDirectories(path))
                {
                    files.AddRange(GetFiles(directory));
                }
            }
            catch (UnauthorizedAccessException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (IOException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return files;
        }


        public static void FindDuplicates(string Path)
        {
            //1st try----------------
            try
            {
                List<string> filesList = new List<string>();
                filesList = new List<string>(GetFiles(Path));

                string[] files = filesList.ToArray();
                List<string> filesums = new List<string>();

                //2nd------------------------------------------
                try
                {
                    foreach (string file in files)
                    {
                        filesums.Add(GetFileSum(file));
                    }
                }
                catch (Exception ex)
                {
                    //a different exception
                    Console.WriteLine(ex.Message);
                    Console.WriteLine("INNER Exception happen in FindDuplicates");
                    Console.ReadLine();
                }
                //2nd try ends-----------------------------------------
                Console.WriteLine("Duplicates: ");

                List<string> duplicates = SearchForDuplicates(filesums);
                PrintDuplicates(filesums, duplicates, files);
            }
            catch (IOException ex)
            {
                //  Console.WriteLine(ex.Message);
                Console.WriteLine("Exception happen in FindDuplicates  IOException");
                // Console.ReadLine();
            }
            catch (Exception ex)
            {
                //a different exception
                Console.WriteLine(ex.Message);
                Console.WriteLine("Exception happen in FindDuplicates");
                Console.ReadLine();
            }
            //1st try ends-----------------------------------------
            Console.ReadLine();
        }


        static List<string> SearchForDuplicates(List<string> sums)
        {
            List<string> duplicates = new List<string>();
            try
            {
                // Search for duplicate files within the given list of sums.
                for (int i = 0; i <= (sums.Count - 2); i++)
                {
                    for (int j = (i + 1); j <= (sums.Count - 2); j++)
                    {
                        if (sums[i] == sums[j])
                            if (!duplicates.Contains(sums[i]))
                                duplicates.Add(sums[i]);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return duplicates;
        }


        //-TO FIX
        //If file in Use(Exc has been thrown) - go to next

        static string GetFileSum(string file)
        {
            //public MD5 md5Hash;
            List<string> HashList = new List<string>();
            try
            {
                using (var sum = MD5.Create())
                using (var stream = File.OpenRead(file))
                    HashList.Add(BitConverter.ToString(sum.ComputeHash(stream)).Replace("-",
                                             "").ToLower());
            }
            catch (IOException ex)
            {
                Console.WriteLine("Exception happen in GetFileSum -  IOException: {0}"
                    , ex.Message);
                Console.ReadLine();
            }
            catch (Exception ex)
            {
                //a different exception
                Console.WriteLine("Exception happen in GetFileSum -  other {0}", ex.Message);
                Console.ReadLine();
            }
            string hash = HashList[0];
            return hash;
        }

        static void PrintDuplicates(List<string> sums, List<string> duplicates, string[] files)
        {
            // Print output.
            foreach (string dupl in duplicates)
            {
                Console.WriteLine("Hashode :{0}\n----------", dupl);

                for (int i = 0; i <= (files.Length - 1); i++)
                {
                    if (sums[i] == dupl)
                        Console.WriteLine(files[i]);
                }
                Console.WriteLine();
            }
            Console.WriteLine("Finished!!");
            Console.WriteLine("Press any key.");
            Console.ReadLine();
        }

        public static void DispMenu(int choise)
        {
            Console.Clear();
            int menuchoice = 0;
            try
            {
                while (menuchoice != 2)
                {
                    Console.ResetColor();
                    Console.Clear();
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine("Welcome to Duplicates finder!!");
                    Console.WriteLine();
                    Console.WriteLine("Please select option below.");
                    Console.WriteLine("Search duplicates in: ");
                    Console.WriteLine();
                    Console.WriteLine("1. - Manually inputed PATH");
                    Console.WriteLine("2. - Exit ");
                    menuchoice = int.Parse(Console.ReadLine());

                    switch (menuchoice)
                    {
                        case 1:
                            ManualInput();
                            break;
                        case 2:
                            break;
                        default:
                            Console.WriteLine("Sorry, invalid selection");
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadLine();
                int MenChoise = 0;
                DispMenu(MenChoise);
            }
        }
    }
}


